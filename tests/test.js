const mongoose = require('mongoose');
const request = require('supertest');
const app = require('../app');
const { Cookie } = require('express-session');
const { proposal } = require('../app/models');
const path = require("path");
const exp = require('constants');
const { deleteOne } = require('../app/models/user.model');



require('dotenv').config();


let token;
let user_id;
let proposalID;
let proposalID2;
/* Connect to the DB before testing */
beforeAll(async () => {
    await mongoose.disconnect();
    mongoose.connect(process.env.MONGO_URL_NONPROD, { useNewUrlParser: true, useUnifiedTopology: true });
})

/* Disconnect from the DB after testing */
afterAll(async () => {
    await mongoose.disconnect();
    await mongoose.connection.close();
})

describe('User as Admin', () => {
    beforeAll(async () => {
        const res = await request(app).post('/login').send({
            username: 'admin01',
            password: 'admin01'
        })
        token = res.body.token;
    })

    afterAll(async () => {
        const res = await request(app).post('/signout').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    })

    it("User as Admin (Fail)", async () => {
        const res = await request(app).post('/login').send({
            username: 'admin01',
            password: 'admin02'
        })
        expect(res.status).toBe(401);
    })

    it("User as Admin (Fail)", async () => {
        const res = await request(app).post('/login').send({
            
            password: 'admin02'
        })
        expect(res.status).toBe(400);
    })

    it('Open Dashboard Panel', async () => {
        const res = await request(app).get('/dashboard').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Dashboard/)
        expect(res.status).toBe(200);
    })

    it('Open Change Password', async () => {
        const res = await request(app).get('/changepassworduserpage').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(200);
        expect(res.text).toMatch(/Old Password/)
    })

    it('Change Password (Fail)', async () => {
        const res = await request(app).post('/changepassworduserpage').send({
            passwordlama: 'admin',
            passwordbaru: 'admin02',
            konfirmasipasswordbaru: 'admin02'
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400);
    })

    it('Change Password (Fail)', async () => {
        const res = await request(app).post('/changepassworduserpage').send({
            passwordlama: 'admin01',
            passwordbaru: 'admin02',
            konfirmasipasswordbaru: 'admin'
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400);
    })

    it('Change Password (Fail)', async () => {
        const res = await request(app).post('/changepassworduserpage').send({
            passwordlama: 'admin',
            passwordbaru: 'admin02',
            konfirmasipasswordbaru: 'admin01'
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400);
    })

    // change the password and then revert the password to the original one
    it('Change Password (Success)', async () => {
        const res = await request(app).post('/changepassworduserpage').send({
            passwordlama: 'admin01',
            passwordbaru: 'admin02',
            konfirmasipasswordbaru: 'admin02'
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    })

    // revert back to original password
    it('Revert to Original Password', async () => {
        const res = await request(app).post('/changepassworduserpage').send({
            passwordlama: 'admin02',
            passwordbaru: 'admin01',
            konfirmasipasswordbaru: 'admin01'
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    })

    // open submitted proposal page and expect the data to be there with status "submitted"
    it('Open Submitted Proposal Page', async () => {
        const res = await request(app).get('/submitted').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Submitted Proposal/)
        expect(res.status).toBe(200);
    })

    it('Open Need Approval Page', async () => {
        const res = await request(app).get('/needapproval').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Need Approval/)
        expect(res.status).toBe(200);
    })

    it('Open Approved Proposal Page', async () => {
        const res = await request(app).get('/approved').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Approved Proposal/)
        expect(res.status).toBe(200);
    })

    it('Open Rejected Proposal Page', async () => {
        const res = await request(app).get('/rejected').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Rejected Proposal/)
        expect(res.status).toBe(200);
    })
    it('Open Redesign Proposal Page', async () => {
        const res = await request(app).get('/redesign').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Redesign Proposal/)
        expect(res.status).toBe(200);
    })

    it('Open On Installation Proposal Page', async () => {
        const res = await request(app).get('/installation').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Installation Proposal/)
        expect(res.status).toBe(200);
    })

    it('Open Closed Proposal Page', async () => {
        const res = await request(app).get('/closed').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Closed Proposal/)
        expect(res.status).toBe(200);
    })

    it('Open QE Report Page', async () => {
        const res = await request(app).get('/qereport').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Segmen/)
        expect(res.text).toMatch(/Nama Alpro/)
        expect(res.text).toMatch(/Bulan/)
        expect(res.status).toBe(200);
    })

    it('Add New Data(Success)', async () => {
        const res = await request(app).post('/adddata').send({
            jenis: "Nama STO",
            namebaru: "SMA"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
        
    })

    it('Add New Data(Fail)', async () => {
        const res = await request(app).post('/adddata').send({
            jenis: "Nama STO",
            namebaru: "SMT"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
        
    })
    it('Add New Data(Fail)', async () => {
        const res = await request(app).post('/adddata').send({
            jenis: "Nama STO",
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
        
    })

    it('Add New Data Page', async () => {
        const res = await request(app).get('/adddata').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Jenis/)
        expect(res.text).toMatch(/Data Baru/)
        expect(res.status).toBe(200)
    }
    )

    // 
    // it('Add New Data(Batch)', async () => {
    //     // use fake timers
    //     // add new data using multipart form data and attach the file on test.csv
    //     const res = await request(app).post('/batch').set('Cookie', [`x-access-token=${token}`]).field("jenis","Nama STO").attach('batch', path.resolve(__dirname, 'test.csv'));
    //     expect(res.status).toBe(200)
    // })


    it("Change Data(Fail)", async () => {
        const res = await request(app).post('/changedata').send({
            jenis: "Nama STO",
            name: "SMA",
            namebaru: "SMA"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
    }
    )

    it("Change Data(Fail)", async () => {
        const res = await request(app).post('/changedata').send({
            jenis: "Nama STO",
            namebaru: "SMA",
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
    }
    )

    it("Change Data(Success)", async () => {
        const res = await request(app).post('/changedata').send({
            jenis: "Nama STO",
            name: "SMA",
            namebaru: "SMK"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    }
    )

    it("Change Data Page", async () => {
        const res = await request(app).get('/changedata').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Jenis/)
        expect(res.text).toMatch(/Data Lama/)
        expect(res.text).toMatch(/Data Baru/)
        expect(res.status).toBe(200)
    }
    )

    it("Delete Data(Fail)", async () => {
        const res = await request(app).post('/deletedata').send(
            {
                jenis: "Nama STO",
            }
        ).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
    })
    
    it("Delete Data(Success)", async () => {
        const res = await request(app).post('/deletedata').send({
            jenis: "Nama STO",
            namebaru: "SMK"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    })

    it("Delete Data Page", async () => {
        const res = await request(app).get('/deletedata').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Jenis/)
        expect(res.text).toMatch(/Data Lama/)
        expect(res.status).toBe(200)
    }
    )

    it("Open User Management", async () => {
        const res = await request(app).get('/userpanel').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/User Management/)
        expect(res.status).toBe(200)
    })

    it("Open Add User", async () => {
        const res = await request(app).get('/adduser').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Username/)
        expect(res.text).toMatch(/Password/)
        expect(res.text).toMatch(/Role/)
        expect(res.text).toMatch(/Nama/)
        expect(res.status).toBe(200)

    })

    it("Add User(Fail)", async () => {
        const res = await request(app).post('/adduser').send({
            username: "admin01",
            password: "admin01",
            roles: "admin",
            name: "admin01",
        }).set('Cookie', [`x-access-token=${token}`]);

        expect(res.status).toBe(400)
    }
    )

    it ("Add User(Fail)", async () => {
        const res = await request(app).post('/adduser').send({
            username: "admin01",
            roles: "admin",
        }).set('Cookie', [`x-access-token=${token}`]);

        expect(res.status).toBe(400)
    }
    )

    it("Open Edit Password", async () => {
        const res = await request(app).get(`/admineditpasswordpage`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/New Password/)  
        expect(res.status).toBe(200)   
    }
    )

    it("Edit Password(Fail)", async () => {
        const res = await request(app).post(`/admineditpasswordpage`).send({
            passwordbaru: "proposer01",
        }).set('Cookie', [`x-access-token=${token}`]);

        expect(res.status).toBe(400)
    }
    )

    it("Edit Password(Success)", async () => {
        const res = await request(app).post(`/admineditpasswordpage`).send({
            user: "proposer01",
            passwordbaru: "proposer01",
        }).set('Cookie', [`x-access-token=${token}`]);

        expect(res.status).toBe(302)
    }
    )

    describe('User Management', () => {
        beforeAll(async () => {
            const res = await request(app).post('/adduser').send({
                username: "tester",
                password: "tester",
                roles: "admin",
                name: "tester",
            }).set('Cookie', [`x-access-token=${token}`]).accept('json');
    
            expect(res.status).toBe(200)
            user_id = res.body.user_id;
        })


        it("Open Edit User", async () => {
            
            const res = await request(app).get(`/edituser/${user_id}`).set('Cookie', [`x-access-token=${token}`]);
            expect(res.text).toMatch(/Update User/)
            expect(res.status).toBe(200)
        }
        )

        it("Edit User(Success)", async () => {
            const res = await request(app).post(`/edituser/${user_id}`).send({
                username: "tester",
                name: "testerBaru",
                roleBaru: "admin",
            }).set('Cookie', [`x-access-token=${token}`]);

            expect(res.status).toBe(302)
        }
        )

        it("Delete User", async () => {
            const res = await request(app).post("/userpanel").send({
                id : user_id
            }).set('Cookie', [`x-access-token=${token}`]);

            expect(res.status).toBe(302)
        }
        )

        

    })
})




describe('User as Proposer', () => {
    beforeAll(async () => {
        const res = await request(app).post('/login').send({
            username: 'proposer01',
            password: 'proposer01'
        })
        token = res.body.token;
    })

    afterAll(async () => {
        const res = await request(app).post('/signout').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    }
    )

    it("User as Proposer (Fail)", async () => {
        const res = await request(app).post('/login').send({
            username: 'proposer01',
            password: 'proposer'
        })
        expect(res.status).toBe(401);
    })

    it("User as Proposer (Fail)", async () => {
        const res = await request(app).post('/login').send({
            
            password: 'proposer02'
        })
        expect(res.status).toBe(400);
    })



    //make new proposal
    it("Make New Proposal(Fail)", async () => {
        const res = await request(app).post('/proposal').send({
            namaSTO: 'SMT',
        }).set('Cookie', [`x-access-token=${token}`]).accept('json');
        expect(res.status).toBe(400);
    })



    it("Make New Proposal(Success)", async () => {
        const res = await request(app).post('/proposal').send({
            namaSTO: 'SMT',
            segmen: 'ODC',
            namaAlpro: 'ODC-SMT-FA',
            jenisQE: 'Perapihan ODC',
            koordinat: '223,123',
            keterangan: 'zzz',
        }).set('Cookie', [`x-access-token=${token}`]).accept('json');
        proposalID = res.body.objectID;
        expect(res.status).toBe(200);
    })

    it("Open My Proposal", async () => {
        const res = await request(app).get('/showproposal').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/My Proposal/)
        expect(res.status).toBe(200);
    }
    )

    it("Open My Specific Proposal", async () => {
        const res = await request(app).get(`/myproposal/${proposalID}`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.text).toMatch(/Detail/)
        expect(res.status).toBe(200);
    }
    )

})

describe('User as Designer', () => {
    beforeAll(async () => {
        const res = await request(app).post('/login').send({
            username: 'designer01',
            password: 'designer01'
        })
        token = res.body.token;
    })
    
    afterAll(async () => {
        const res = await request(app).post('/signout').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    }
    )

    it("User as Designer (Fail)", async () => {
        const res = await request(app).post('/login').send({
            username: 'designer01',
            password: 'designer'
        })
        expect(res.status).toBe(401);
    })

    it("User as Designer (Fail)", async () => {
        const res = await request(app).post('/login').send({
            
            password: 'designer02'
        })
        expect(res.status).toBe(400);
    })


    it("Open Inbox", async () => {
        const res = await request(app).get('/designer').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Inbox/)
        expect(res.status).toBe(200);
    }

    )

    it("Open Specific Proposal", async () => {
        const res = await request(app).get(`/detail/${proposalID}/design`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.status).toBe(200);
    }

    )

    it("Open Specific Proposal Design and RAB Page", async () => {
        const res = await request(app).get(`/detail/${proposalID}/design/upload`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Design/)
        expect(res.text).toMatch(/RAB/)
        expect(res.text).toMatch(/Nilai RAB/)
        expect(res.status).toBe(200);
    }
    )

    // it("Upload Design and RAB(Success)", async () => {
    //     const res = await request(app).post(`/detail/${proposalID}/design/upload`)
    //                     .set('Cookie', [`x-access-token=${token}`])
    //                     .attach('design', path.resolve(__dirname, 'test.csv'))
    //                     .attach('rab', path.resolve(__dirname, 'test.csv'))
    //                     .field('nilairab', 777)
    //     expect(res.status).toBe(302)

    // }
    // )
    
    it("Upload Design and RAB(Fail)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/design/upload`)
                        .set('Cookie', [`x-access-token=${token}`])
                        .field('nilairab', 777)
        expect(res.status).toBe(500)
    }
    )

    it("Open My Design", async () => {
        const res = await request(app).get('/submittedproposal').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.text).toMatch(/Submitted Proposal/)
        expect(res.status).toBe(200)
    }
    )
})

describe('User as Approver', () => {
    beforeAll(async () => {
        const res = await request(app).post('/login').send({
            username: 'approver01',
            password: 'approver01'
        })
        token = res.body.token;
    })

    afterAll(async () => {
        const res = await request(app).post('/signout').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    }
    )
    
    it("User as Approver (Fail)", async () => {
        const res = await request(app).post('/login').send({
            username: 'approver01',
            password: 'approver'
        })
        expect(res.status).toBe(401);
    })

    it("User as Approver (Fail)", async () => {
        const res = await request(app).post('/login').send({
            
            password: 'approver02'
        })
        expect(res.status).toBe(400);
    })


    it("Open Inbox", async () => {
        const res = await request(app).get('/approver').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Need Approval/)
        expect(res.status).toBe(200);
    }
    )

    it("Open Specific Proposal", async () => {
        const res = await request(app).get(`/detail/${proposalID}/approver`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.status).toBe(200);
    }
    )

    it('Download Design File', async () => {
        const res = await request(app).get(`/detail/${proposalID}/design/download`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(200)
    })

    it("Download RAB File", async () => {
        const res = await request(app).get(`/detail/${proposalID}/rab/download`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(200)
    }
    )

    it("Redesign Proposal", async () => {
        const res = await request(app).post(`/proposal/approver/redesign/${proposalID}`).send({
            catatan: "test redesign"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    }
    )

    it("Reject Proposal", async () => {
        const res = await  request(app).post(`/proposal/approver/reject/${proposalID}`).set('Cookie', [`x-access-token=${token}`])
        expect(res.status).toBe(302)
    }
    )

    it("Approve Proposal", async () => {
        const res = await request(app).post(`/detail/${proposalID}/approver`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    }
    )
})

describe('User as Executor', () => {
    beforeAll(async () => {
        const res = await request(app).post('/login').send({
            username: 'executor01',
            password: 'executor01'
        })
        token = res.body.token;
    })

    afterAll(async () => {
        const res = await request(app).post('/signout').set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302);
    }
    )

    it("User as Executor (Fail)", async () => {
        const res = await request(app).post('/login').send({
            username: 'executor01',
            password: 'executor'
        })
        expect(res.status).toBe(401);
    })

    it("User as Approver (Fail)", async () => {
        const res = await request(app).post('/login').send({
            
            password: 'executor02'
        })
        expect(res.status).toBe(400);
    })


    it("Open Inbox", async () => {
        const res = await request(app).get('/executor').set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Approved Proposal/)
        expect(res.status).toBe(200);
    }
    )

    it("Open Specific Proposal", async () => {
        const res = await request(app).get(`/detail/${proposalID}/executor`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.status).toBe(200);
    }
    )

    it('Download Design File', async () => {
        const res = await request(app).get(`/detail/${proposalID}/design/download`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(200)
    })

    it("Download RAB File", async () => {
        const res = await request(app).get(`/detail/${proposalID}/rab/download`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(200)
    }
    )

    it("Choose Mitra Page", async () => {
        const res = await request(app).get(`/detail/${proposalID}/executor/pilihmitra`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Choose Mitra/)
        expect(res.status).toBe(200)
    }
    )

    it("Choose Mitra(Fail)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/executor/pilihmitra`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
    }
    )

    it("Choose Mitra(Success)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/executor/pilihmitra`).send({
            mitra: "Mitra 1"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    }
    )

    it("Change Mitra Page", async () => {   
        const res = await request(app).get(`/detail/${proposalID}/executor/gantimitra`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/Mitra Baru/)
        expect(res.status).toBe(200)
    }
    )

    it("Change Mitra(Fail)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/executor/gantimitra`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(400)
    }
    )

    it("Change Mitra(Success)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/executor/gantimitra`).send({
            mitra: "Mitra 2"
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(302)
    }
    )

    it("Close Order Page", async () => {
        const res = await request(app).get(`/detail/${proposalID}/executor/upload`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/As Built Drawing/)
        expect(res.status).toBe(200)
    }
    )

    it("Close Order(Fail)", async () => {
        const res = await request(app).post(`/detail/${proposalID}/executor/upload`).set('Cookie', [`x-access-token=${token}`]);
        expect(res.status).toBe(500)
    }
    )

    // it("Close Order(Success)", async () => {
    //     const res = await request(app).post(`/detail/${proposalID}/executor/upload`)
    //                     .set('Cookie', [`x-access-token=${token}`])
    //                     .attach('designevidence', path.resolve(__dirname, 'test.csv'))
    //                     .attach('rabevidence', path.resolve(__dirname, 'test.csv'))
    //                     .field('nilairabevidence', '999')
    //     expect(res.status).toBe(302)
    // }
    // )


    
    it('Open QE Report List Page(Success)', async () => {
        const res = await request(app).post('/qereportlist').send({
            namaSTO: 'SMT',
            segmen: "ODC",
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/SMT-02/)
        expect(res.status).toBe(200);
    })

    it('Open QE Report List Page(No data)', async () => {
        const res = await request(app).post('/qereportlist').send({
            namaSTO: 'SMT'
            
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/No data available/)
        expect(res.status).toBe(200);
    })




    it('Open QE Report List Page(Success)', async () => {
        const res = await request(app).post('/qereportlist').send({
            namaSTO: 'SMT',
            segmen: "ODC",
            namaalpro: "ODC-SMT-FA",
            jenisQE: "Perapihan ODC",
            bulan: 'Nov'
            
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/SMT/)
        expect(res.text).toMatch(/ODC/)
        expect(res.text).toMatch(/ODC-SMT-FA/)
        expect(res.text).toMatch(/Nov/)
        expect(res.text).toMatch(/Perapihan ODC/)
        expect(res.status).toBe(200);
    })
    it('Open QE Report List Page(Success)', async () => {
        const res = await request(app).post('/qereportlist').send({
            namaSTO: "",
            segmen: "",
            namaalpro: "",
            jenisQE: "",
            bulan: ""
            
        }).set('Cookie', [`x-access-token=${token}`]);
        expect(res.text).toMatch(/No data available/)
        expect(res.status).toBe(200);
        
    })

})


    




