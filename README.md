# Peduli Akses

Peduli Akses merupakan aplikasi yang dikembangkan untuk memenuhi kebutuhan divisi _Access Maintenance and Quality Enhancement_ PT Telkom Indonesia Witel Semarang untuk mempercepat proses pengajuan usulan perbaikan agar lebih efektif dan efisien.

## Kebutuhan Instalasi

Pastikan telah menginstall kebutuhan yang digunakan untuk menjalankan aplikasi ini, antara lain :

1. Visual Studio Code / code editor lainnya.
2. NodeJS

## Jalankan di Perangkat Lokal

1. Buka file program pada code editor.
2. Buka terminal.
3. Install package yang diperlukan.

```bash
  npm install
```

4. Jalankan local server.

```bash
  npm run dev
```

5. Buka Web Browser.
6. Buka laman [Localhost](https://localhost:8080) dengan port 8080.

7. Masukkan akun yang terdaftar untuk melakukan autentikasi.

## Daftar Akun

Berikut daftar akun yang tersedia untuk login kedalam aplikasi :

- Proposer

```
Username : proposer01
Password : proposer02
```

- Designer

```
Username : designer01
Password : designer01
```

- Approver

```
Username : approver01
Password : approver01
```

- Executor

```
Username : executor01
Password : executor01
```

- Admin

```
Username : admin01
Password : admin01
```
